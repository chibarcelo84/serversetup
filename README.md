# serverSetup Script

These collection of scripts aims to help people set up their servers to run WordPress easily.  

A section of this script includes a tweaked and modified version of [Quick LEMP stack Installer script](https://github.com/thamaraiselvam/LEMP-Installer).
All credits to the owner for this very useful script!  

## Things you need to know

1. The script has been tested and verified working in Ubuntu 18.04 Bionic Beaver and Ubuntu 16.04 Xenial Xerus
2. Tested in Google Cloud.
3. Tested using php7.4
4. Tested SSL using LetsEncrypt (Certbot)
5. Untested with other Ubuntu versions or other Linux Distros

## Getting started

1. Make sure you already have your domain name and is pointed to your server's ip.
2. Prepare your credentials. (usernames,passwords)

## Create a config file

Copy the code below and paste inside the terminal.  

```bash
echo \
"#Enter required details:    
#Domain Name    
yourdomain.com
#Email for SSL    
youremail@email.com
#Root password for Database    
yourrootpassword
#Database name    
yourdatabasename
#Database user    
yourdatabaseuser
#Database user password    
yourdatabasepassword
#WARNING! Change only if necessary!    
#phpVersion    
7.4" >> config
```

Open nano editor.  
``sudo nano config``

Update the config file with the correct credentials. In the Nano editor, Save by pressing ``Ctrl+x`` and then ``Y``. Press ``Enter`` to exit.

## Let's go set up our server!

In the home directory, create the *start.sh* file using the Nano editor.  
``sudo nano start.sh``

Copy the code below and paste inside the editor.  

```bash
#!/bin/bash   
git clone https://chibarcelo84@bitbucket.org/chibarcelo84/serversetup.git
sudo cp config serversetup/config    
cd serversetup    
sudo chmod +x setupServer.sh    
./setupServer.sh    
cd ~    
sudo rm config    
```

Exit and save the file by pressing ``Ctrl + x``. Press ``Y`` to confirm saving and press ``Enter`` to save and exit.

Make the script(s) executable.    
``sudo chmod +x start.sh``

Run the script!  
``./start.sh``  

This script will generate a file containing your credentials. Keep it safe somewhere, then delete it along with the script folder from your server.  
``sudo cat ~/servercredentials.txt``  
``sudo rm -rf ~/servercredentials.txt ~/serversetup``

Clear your history (optional)  
``history -c``

When the script is done, you can check your website and you should be able to view the WordPress installation page!

## Rebuilding

If for some reason you come across some issues, you can undo some changes (but not all) and allow you to retry again.  
**You may need to clone this repository again as the script folder will be deleted after executing the start.sh script.**

Go to the script folder  
``cd ~/serversetup``

Make sure that these files are executable.  
``sudo chmod +x startrollback.sh  ``  
``sudo chmod +x rebuild.sh``

Run the rebuild script.  
``./rebuild.sh``

The rebuild script will undo changes and restart the setup process. Always make sure that the *config* file has the correct credentials!

## Rollback

If you want to execute rollback manually, you may do so.

Go to the script folder  
``cd ~/serversetup``  

Make sure that the rollback script file is executable.  
``sudo chmod +x startrollback.sh``

Run the rollback script.  
``./startrollback.sh``

After the rollback, you may delete the folder. (Recommended)  
``sudo rm -rf ~/serversetup``

## Thank you

I hope this script would be of help to others who are specifically in need of this. This may not be the best script out there but THIS is the one script that helped me automate my server setup for WordPress.

To your success!

[Chi](https://chibarcelo.com)
