#!/bin/bash

init() {

    echo -e "$(tput setaf 0)$(tput setab 3)This action will remove significant files from your server!$(tput sgr 0)"
    read -p "Are you sure you want to continue? Press "Y" to continue or press any key to abort. " userChoice

    if [[ "$userChoice" == "y" ]]; then
        echo -e "Starting rollback..."
        sudo chmod +x rollback/rollback.sh
        sudo rollback/rollback.sh
        # Rollback done
        echo -e "$(tput setaf 0)$(tput setab 3)Rollback Success!$(tput sgr 0)"

    elif [[ "$userChoice" == "Y" ]]; then
        echo -e "Starting rollback..."
        sudo chmod +x rollback/rollback.sh
        sudo rollback/rollback.sh
        # Rollback done
        echo -e "$(tput setaf 0)$(tput setab 3)Rollback Success!$(tput sgr 0)"

    else
        echo "Rollback aborted."
        exit 1
    fi


}

init
