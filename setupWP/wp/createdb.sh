#!/bin/bash
# DB Variables -- Don't Change!
dbRootPass=`awk 'NR==7' config`
dbName=`awk 'NR==9' config`
dbUser=`awk 'NR==11' config`
dbPassword=`awk 'NR==13' config`

# Create CNF file
sudo rm -rf /etc/my.cnf

echo \
"!includedir /etc/mysql/conf.d/
[client]
user=root
password=$dbRootPass" | sudo tee -a /etc/my.cnf

# Creating database for site
sudo mysql -uroot -e "CREATE DATABASE $dbName DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;CREATE USER $dbUser@localhost identified by '$dbPassword';GRANT ALL ON $dbName.* to $dbUser@localhost WITH GRANT OPTION;FLUSH PRIVILEGES;"

# Confirmation message
echo -e "Database created successfully!"
