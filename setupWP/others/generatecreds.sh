#!/bin/bash
# Creds Variables -- Don't Change!
domain=`awk 'NR==3' config`
sslEmail=`awk 'NR==5' config`
sslEmail=`awk 'NR==5' config`
dbRootPass=`awk 'NR==7' config`
dbName=`awk 'NR==9' config`
dbUser=`awk 'NR==11' config`
dbPassword=`awk 'NR==13' config`

touch ~/servercredentials.txt

echo \
"These are your server credentials.
Keep this in a safe place!

  Domain: $domain
  sslEmail: $sslEmail
  dbRootPass: $dbRootPass
  dbName: $dbName
  dbUser: $dbUser
  dbPassword: $dbPassword" | sudo tee -a ~/servercredentials.txt
