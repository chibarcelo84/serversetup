#!/bin/bash

# Make sure your config is in your home directory
sudo cp ~/config serversetup/config
sudo chmod +x serversetup/setupServer.sh serversetup/startrollback.sh
cd serversetup
~/serversetup/startrollback.sh
~/serversetup/setupServer.sh
