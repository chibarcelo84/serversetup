#!/bin/bash
# Config Variables
domain=`awk 'NR==3' config`

# Prepare folder path
sudo mkdir /var/www/$domain

# Download latest WordPress
cd /tmp
sudo curl -LO https://wordpress.org/latest.tar.gz
sudo tar xzvf latest.tar.gz

# Generate Salt Keys (For Security)
sudo curl -s https://api.wordpress.org/secret-key/1.1/salt/ | tee -a salt.txt

# Generate wp-config file
sudo ~/serversetup/setupWP/wp/generatewpconfig.sh

sudo cp -a /tmp/wordpress/. /var/www/$domain
sudo rm -rf /tmp/salt.txt

# Update folder ownership
sudo chown -R www-data:www-data /var/www/$domain

cd ~/serversetup
