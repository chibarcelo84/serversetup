#!/bin/bash

# The script will fail at the first error encountered
set -e

# Initialization
sudo chmod +x setupWP/initialize.sh
setupWP/initialize.sh


# Installing LEMP
echo -e "Installing LEMP..."

dbRootPass=`awk 'NR==7' config`
setupWP/lemp.sh $dbRootPass

# Install PHP Extensions
setupWP/wp/extensions.sh

# Update NGINX for WordPress
setupWP/wp/nginxupdate.sh

# Install SSL Certificate
setupWP/installers/setupssl.sh

# Create WP DATABASE
setupWP/wp/createdb.sh

# Install WordPress
setupWP/wp/installwp.sh

# Generating server credentials text file
setupWP/others/generatecreds.sh

echo -e "$(tput setaf 0)$(tput setab 3)IMPORTANT!$(tput sgr 0)"
echo -e "Keep the file servercredentials.txt secure!"

# End of Installation script
echo -e "$(tput setaf 0)$(tput setab 3)Your server has been set up successfully!$(tput sgr 0)"

domain=`awk 'NR==3' config`
echo -e "You may now visit your website here: "
echo -e "$(tput setaf 0)$(tput setab 3)https://$domain$(tput sgr 0)"

# Folder deletion
cd ~
history -c
