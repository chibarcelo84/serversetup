#!/bin/bash

# Install PHP Extensions
echo -e "Installing PHP Extensions..."
sudo apt update
sudo apt install -y php-curl php-gd php-intl php-mbstring php-soap php-xml php-xmlrpc php-zip
sudo systemctl restart php$phpVersion-fpm

echo -e "PHP Extensions installed successfully!"
