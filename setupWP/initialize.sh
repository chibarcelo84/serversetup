#!/bin/bash

init() {

    echo -e "Initializing files..."
    sudo chmod +x setupWP/lemp.sh
    sudo chmod +x setupWP/wp/nginxupdate.sh
    sudo chmod +x setupWP/wp/createdb.sh
    sudo chmod +x setupWP/wp/extensions.sh
    sudo chmod +x setupWP/wp/generatewpconfig.sh
    sudo chmod +x setupWP/installers/setupssl.sh
    sudo chmod +x setupWP/wp/installwp.sh
    sudo chmod +x setupWP/others/generatecreds.sh
    sudo chmod +x rollback/rollback.sh

    echo -e "Files initialized!"
}

init
