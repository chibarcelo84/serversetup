#!/bin/bash

# Nginx Variables -- Don't Change!
uri='$uri'
args='$args'
is_args='$is_args'
$='$'
domain=`awk 'NR==3' config`
phpVersion=`awk 'NR==16' config`

# Update NGINX Default File
echo -e "Updating NGINX config..."
sudo rm -rf /etc/nginx/sites-available/default
sudo touch /etc/nginx/sites-available/default

echo \
"#Config - $domain

server {
    
    root /var/www/$domain;
    index index.php;

    server_name $domain;

    location / {
        try_files $uri $uri/ /index.php$is_args$args;
    }
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php$phpVersion-fpm.sock;
    }
    location ~ /\.ht {
        deny all;
    }
    location = /favicon.ico { log_not_found off; access_log off; }
    location = /robots.txt { log_not_found off; access_log off; allow all; }
    location ~* \.(css|gif|ico|jpeg|jpg|js|png)$ {
        expires max;
        log_not_found off;
    }
}" | sudo tee -a  /etc/nginx/sites-available/default

sudo service nginx reload
sudo service nginx restart

echo -e "NGINX successfully updated!"