#!/bin/bash
# SSL Variables -- Don't Change!
domain=`awk 'NR==3' config`
sslEmail=`awk 'NR==5' config`

# Install Certbot dependencies
sudo apt-get update -y
sudo apt-get install software-properties-common -y
sudo add-apt-repository ppa:certbot/certbot -y
sudo apt-get update -y
sudo apt-get install python-certbot-nginx -y

# Install Let's Encrypt SSL
echo -e "Installing Let's Encrypt SSL..."

sudo certbot run -n --nginx --agree-tos -d $domain  -m  $sslEmail  --redirect
echo -e "SSL installed!"
