#!/bin/bash

# Removing MySQL
sudo apt purge mysql-server mysql-client mysql-common mysql-server-core-* mysql-client-core-* mysql-client -y
sudo rm -rf /etc/mysql /var/lib/mysql /var/log/mysql
sudo rm -rf ~/.my.cnf

# Removing NGINX
sudo apt purge nginx* -y
sudo rm -rf /etc/nginx

#Remove PHP
sudo apt purge php* -y
sudo rm -rf /etc/php$phpVersion

#Remove WordPress
sudo rm -rf /tmp/wordpress
sudo rm -rf /var/www/$domain

# Remove LetsEncrypt
sudo apt purge certbot* -y
sudo rm -rf /etc/LetsEncrypt

sudo apt autoclean -y
sudo apt autoremove -y
