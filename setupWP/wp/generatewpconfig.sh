#!/bin/bash

# Variables
table_prefix='$table_prefix'
dbName=`awk 'NR==9' ~/serversetup/config`
dbUser=`awk 'NR==11' ~/serversetup/config`
dbPassword=`awk 'NR==13' ~/serversetup/config`
saltkeys=`cat /tmp/salt.txt`

# Update wp-config file
sudo touch /tmp/wordpress/wp-config.php
echo \
"<?php

/** Database Details */
define( 'DB_NAME', '$dbName' );
define( 'DB_USER', '$dbUser' );
define( 'DB_PASSWORD', '$dbPassword' );
define( 'DB_HOST', 'localhost' );
define( 'DB_CHARSET', 'utf8mb4' );
define( 'DB_COLLATE', '' );

/** Authentication Unique Keys and Salts.  */
$saltkeys

$table_prefix = 'wp_';
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
       define( 'ABSPATH', __DIR__ . '/' );
}
/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';" | sudo tee -a /tmp/wordpress/wp-config.php

cd /tmp
